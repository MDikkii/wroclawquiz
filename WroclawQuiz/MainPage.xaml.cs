﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using System.Windows.Media;

namespace WroclawQuiz
{
    public partial class MainPage : PhoneApplicationPage
    {
        //Random _r;
        public MainPage()
        {
           
            InitializeComponent();

            FeedbackOverlay.VisibilityChanged += FeedbackOverlay_VisibilityChanged;


            //_r = new Random();
            //DispatcherTimer timmer = new DispatcherTimer();
            //timmer.Interval = new TimeSpan(10000);
            //timmer.Tick += timmer_Tick;
            //timmer.Start();

        }

        private void FeedbackOverlay_VisibilityChanged(object sender, EventArgs e)
        {
            if (ApplicationBar != null)
            {
                ApplicationBar.IsVisible = (FeedbackOverlay.Visibility != Visibility.Visible);
            }
        }

        private void playButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/QuestionPage.xaml",UriKind.Relative));
        }



        


        // kolory plus obsługa taimeru (disco ball)
        //Color GetRandomColor()
        //{
        //    int random = _r.Next(9);

        //    switch (random)
        //    {
        //        case 0: return Colors.Blue;
        //        case 1: return Colors.Magenta;
        //        case 2: return Colors.Red;
        //        case 3: return Colors.White;
        //        case 4: return Colors.Yellow;
        //        case 5: return Colors.Purple;
        //        case 6: return Colors.Gray;
        //        case 7: return Colors.Cyan;
        //        case 8: return Colors.Orange;
        //    }
        //    return Colors.Green;
        //}

        //void timmer_Tick(object sender, EventArgs e)
        //{
        //    LayoutRoot.Background = new SolidColorBrush(GetRandomColor());
        //    settignsButton.Background = new SolidColorBrush(GetRandomColor());
        //    leaderBoardButton.Background = new SolidColorBrush(GetRandomColor());
        //}
    }
}