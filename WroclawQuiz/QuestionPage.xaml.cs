﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using System.Globalization;

namespace WroclawQuiz
{
    public partial class QuestionPage : PhoneApplicationPage
    {
        private List<Question> allQuestions;
        private int ALL_QUESTION_COUNT;
        private List<Question> playQuestions;
        private int PLAY_QUESTIONS_COUNT = 10;
        private int goodAnswer = 0;

        private Question currentQuestion = null;
        private int currentQuestionCount = 0;

        //ModelView _viewModel;
        //bool _isNewPageInstance = false;

        //Page Constructor        
        public QuestionPage()
        {

            InitializeComponent();

            allQuestions = new List<Question>();

            if (CultureInfo.CurrentCulture.Name == "pl-PL" || CultureInfo.CurrentCulture.Name == "pl")
            {
                //Public transport
                allQuestions.Add(new Question("Ile jest stałych lini tramwajowych?", "/Assets/Photos/tramwaj.jpg", "22", new[] { "28", "25" }));
                allQuestions.Add(new Question("Jakiej marki są najnowsze tramwaje?", "/Assets/Photos/tramwaj.jpg", "Skoda", new[] { "Volvo", "Mercedes" }));
                allQuestions.Add(new Question("Jak nazywa się jeden z zabytkowych tramwajów?", "/Assets/Photos/przystanek_tramw.jpg", "Jaś i Małgosia", new[] { "Bolek i Lolek", "Wrocławiak" }));
                allQuestions.Add(new Question("Ile jest nocnych lini autobusowych?", "/Assets/Photos/Wroclaw.png", "13", new[] { "11", "25" }));
                allQuestions.Add(new Question("Jaki jest najwyższy numer lini tramwajowej?", "/Assets/Photos/przystanek_tramw.jpg", "33", new[] { "26", "24" }));
                allQuestions.Add(new Question("Czyje imię nosi wrocławski port lotniczy?", "/Assets/Photos/Wroclaw.png", "Mikołaja Kopernika", new[] { "Lecha Wałęsy", "Jana Kochanowskiego" }));
                allQuestions.Add(new Question("Ile dworców PKP znajduje się na terenie Wrocławia?", "/Assets/Photos/dworzec.jpg", "19", new[] { "10", "14" }));
                allQuestions.Add(new Question("Ile kosztuje bilet ulgowy na jednorazowy przejazd autobusem normalnym?", "/Assets/Photos/Wroclaw.png", "1,50zł", new[] { "1,00zł", "1,10zł" }));
                allQuestions.Add(new Question("Ile kosztuje bilet normalny na przejazd autobusem pospiesznym?", "/Assets/Photos/Wroclaw.png", "3,20zł", new[] { "3,00zł", "3,10zł" }));
                allQuestions.Add(new Question("Od którego roku funkcjonuje Wrocławski Rower Miejski?", "/Assets/Photos/wrm.jpg", "2011", new[] { "2009", "2012" }));

                //Curiosity
                allQuestions.Add(new Question("Ile jest Wrocławskich krasnali?", "/Assets/Photos/Wroclaw.png", "ok. 270", new[] { "ok. 220", "ok. 145" }));
                allQuestions.Add(new Question("Ilu mieszkańców ma Wrocław?", "/Assets/Photos/Wroclaw.png", "630 tys.", new[] { "580 tys.", "600 tys." }));
                allQuestions.Add(new Question("Jaki budynek stał na miejscu Sky Tower?", "/Assets/Photos/sky_tower.JPG", "Poltegor", new[] { "Biały Pawilon", "Nic tam nie było" }));
                allQuestions.Add(new Question("Na którym moście wieszane są kłódki?", "/Assets/Photos/most_uni.jpg", "Tumskim", new[] { "Uniwersyteckim", "Zwierzynieckim" }));
                allQuestions.Add(new Question("Ile mostów jest we Wrocławiu?", "/Assets/Photos/most_uni.JPG", "ok.120-130", new[] { "ok.300-310", "ok.220-230" }));
                allQuestions.Add(new Question("Na którym piętrze znajduje się taras widokowy w Sky Tower?", "/Assets/Photos/sky_tower.JPG", "49", new[] { "50", "47" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/Wroclaw.png", "192", new[] { "124", "133" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));

                //Monuments
                allQuestions.Add(new Question("Co jest widoczne na zdjęciu?", "/Assets/Photos/katedra.JPG", "Katedra św. Jana Chrzciciela", new[] { "Kościół NMP na Piasku", "Kościół Świętego Krzyża" }));
                allQuestions.Add(new Question("Który z poetów siedzi na rynku?", "/Assets/Photos/fredro.JPG", "Fredro", new[] { "Krasicki", "Słowacki" }));
                allQuestions.Add(new Question("Z którego roku pochodzi dzwon w wieży ratusza?", "/Assets/Photos/ratusz.JPG", "1368", new[] { "1544", "1829" }));
                allQuestions.Add(new Question("Ile stopni prowadzi na górę wieży widokowej kościoła garnizonowego?", "/Assets/Photos/wieza.JPG", "302", new[] { "278", "294" }));
                allQuestions.Add(new Question("Od rocznicy jakiego wydarzenia bierze nazwę Hala Stulecia?", "/Assets/Photos/hala_stulecia.JPG", "Bitwy pod Lipskiem", new[] { "Wjazdu Napoleona", "Otwarcia ZOO" }));
                allQuestions.Add(new Question("Co to za budynek?", "/Assets/Photos/panorama_roclawicka.JPG", "Panorama Rocławicka", new[] { "Muzem Miejskie", "Hala sportowa" }));
                allQuestions.Add(new Question("Co mieści się w tym budynku?", "/Assets/Photos/urzad_woj.JPG", "Urząd Wojewódzki", new[] { "Sejm", "Urząd Miasta Wrocław" }));
                allQuestions.Add(new Question("Czyj to pomnik?", "/Assets/Photos/jan.jpg", "Jana XXIII", new[] { "Jana Pawła II", "Piusa XII" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));

                //Culture
                allQuestions.Add(new Question("Ile kinowych multiplexów jest we Wrocławiu?", "/Assets/Photos/Wroclaw.png", "5", new[] { "10", "2" }));
                allQuestions.Add(new Question("Jak nazywa się wrocławski teatr muzyczny?", "/Assets/Photos/capitol.jpg", "Capitol", new[] { "Broadway", "Grease" }));
                allQuestions.Add(new Question("Przy jakiej ulicy mieści się Kino Nowe Horyzonty?", "/Assets/Photos/Wroclaw.png", "Kazimierza Wielkiego", new[] { "Józefa Poniatowskiego", "Legnicka" }));
                allQuestions.Add(new Question("Jaki utwór gramy podczas bicia rekordu Guinessa na rynku?", "/Assets/Photos/Wroclaw.png", "Hey Joe", new[] { "Bold as Love", "Fire" }));
                allQuestions.Add(new Question("Który z festiwali odbywa się we Wrocławiu?", "/Assets/Photos/Wroclaw.png", "Jazz nad Odrą", new[] { "Live Festival", "Jazz Jamboree" }));
                allQuestions.Add(new Question("Co ginie z fontanny na placu Uniwersyteckim?", "/Assets/Photos/szermierz.JPG", "Szpada", new[] { "Dysk", "Pióro" }));
                allQuestions.Add(new Question("W którym roku Wrocław będzie Europejską Stolicą Kultury?", "/Assets/Photos/stolica_kult.jpg", "2016", new[] { "2019", "2017" }));
                allQuestions.Add(new Question("Co to za budynek?", "/Assets/Photos/opera.JPG", "Opera", new[] { "Teatr", "Kino" }));
                allQuestions.Add(new Question("W którym miesiącu zacznie się festiwal Szanty we Wrocławiu?", "/Assets/Photos/szanty.JPG", "Luty", new[] { "Marzec", "Lipiec" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/PocztaPolska.JPG", "?", new[] { "124", "133" }));

                //Sport
                allQuestions.Add(new Question("Ile jest miejsc na stadionie miejskim?", "/Assets/Photos/Wroclaw.png", "ok. 45 tys.", new[] { "ok. 22 tys.", "ok. 38 tys." }));
                allQuestions.Add(new Question("Ile Mistrzostw Polski ma na koncie WKS (koszykówka)?", "/Assets/Photos/Wroclaw.png", "17", new[] { "11", "15" }));
                allQuestions.Add(new Question("W którym roku WKS (piłka nożna) zdobył Mistrzostwo Polski?", "/Assets/Photos/Wroclaw.png", "2012", new[] { "2011", "2009" }));
                allQuestions.Add(new Question("Ile Wicemistrzostw Polski ma na koncie WKS (piłka nożna)?", "/Assets/Photos/Wroclaw.png", "3", new[] { "9", "2" }));
                allQuestions.Add(new Question("Który z bramkarzy WKS zazwyczaj występuje w pierwszej jedynastce?", "/Assets/Photos/Wroclaw.png", "M. Kelemen", new[] { "W. Pawłowski", "J. Wrąbel" }));
                allQuestions.Add(new Question("Od którego sezonu Impel Wrocław (siatkówka) gra w ekstralidze?", "/Assets/Photos/Wroclaw.JPG", "2012/2013", new[] { "2011/2012", "2009/2010" }));
                //allQuestions.Add(new Question("W którym roku Wrocław będzie Europejską Stolicą Kultury?", "/Assets/Photos/stolica_kult.jpg", "2016", new[] { "2019", "2017" }));
                //allQuestions.Add(new Question("Co to za budynek?", "/Assets/Photos/opera.JPG", "Opera", new[] { "Teatr", "Kino" }));
                //allQuestions.Add(new Question("W którym miesiącu zacznie się festiwal Szanty we Wrocławiu?", "/Assets/Photos/PocztaPolska.JPG", "Lutym", new[] { "Marcu", "Lipcu" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/PocztaPolska.JPG", "?", new[] { "124", "133" }));
            }
            else
            {
                //Public transport
                allQuestions.Add(new Question("How many tram-lines are in Wroclaw?", "/Assets/Photos/tramwaj.jpg", "22", new[] { "28", "25" }));
                allQuestions.Add(new Question("Which company produces newest Wroclaw's trams?", "/Assets/Photos/tramwaj.jpg", "Skoda", new[] { "Volvo", "Mercedes" }));
                allQuestions.Add(new Question("What is vintage tram's name?", "/Assets/Photos/przystanek_tramw.jpg", "Jaś i Małgosia", new[] { "Bolek i Lolek", "Wrocławiak" }));
                allQuestions.Add(new Question("How many bus lines are in Wroclaw?", "/Assets/Photos/Wroclaw.png", "13", new[] { "11", "25" }));
                allQuestions.Add(new Question("Which is the highest number of tram-line?", "/Assets/Photos/przystanek_tramw.jpg", "33", new[] { "26", "24" }));
                allQuestions.Add(new Question("What is a name of Wroclaw's airport?", "/Assets/Photos/Wroclaw.png", "Mikołaja Kopernika", new[] { "Lecha Wałęsy", "Jana Kochanowskiego" }));
                //allQuestions.Add(new Question("Ile dworców PKP znajduje się na terenie Wrocławia?", "/Assets/Photos/dworzec.jpg", "19", new[] { "10", "14" }));
                //allQuestions.Add(new Question("Ile kosztuje bilet ulgowy na jednorazowy przejazd autobusem normalnym?", "/Assets/Photos/Wroclaw.png", "1,50zł", new[] { "1,00zł", "1,10zł" }));
                //allQuestions.Add(new Question("Ile kosztuje bilet normalny na przejazd autobusem pospiesznym?", "/Assets/Photos/Wroclaw.png", "3,20zł", new[] { "3,00zł", "3,10zł" }));
                //allQuestions.Add(new Question("Od którego roku funkcjonuje Wrocławski Rower Miejski?", "/Assets/Photos/wrm.jpg", "2011", new[] { "2009", "2012" }));

                //Curiosity
                allQuestions.Add(new Question("How many Wroclaw's dwarfs are there?", "/Assets/Photos/Wroclaw.png", "ok. 270", new[] { "ok. 220", "ok. 145" }));
                allQuestions.Add(new Question("How many people live in Wroclaw?", "/Assets/Photos/Wroclaw.png", "630 tys.", new[] { "580 tys.", "600 tys." }));
                allQuestions.Add(new Question("Which building had been before Sky Tower was built?", "/Assets/Photos/sky_tower.JPG", "Poltegor", new[] { "Biały Pawilon", "Nic tam nie było" }));
                //allQuestions.Add(new Question("Na którym moście wieszane są kłódki?", "/Assets/Photos/most_uni.jpg", "Tumskim", new[] { "Uniwersyteckim", "Zwierzynieckim" }));
                allQuestions.Add(new Question("How many bridges are there in Wroclaw?", "/Assets/Photos/most_uni.JPG", "ok.120-130", new[] { "ok.300-310", "ok.220-230" }));
                //allQuestions.Add(new Question("Na którym piętrze znajduje się taras widokowy w Sky Tower?", "/Assets/Photos/sky_tower.JPG", "49", new[] { "50", "47" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/Wroclaw.png", "192", new[] { "124", "133" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));

                //Monuments
                allQuestions.Add(new Question("What is on the picture?", "/Assets/Photos/katedra.JPG", "Katedra św. Jana Chrzciciela", new[] { "Kościół NMP na Piasku", "Kościół Świętego Krzyża" }));
                //allQuestions.Add(new Question("Which of the poets sits on the Market Square?", "/Assets/Photos/fredro.JPG", "Fredro", new[] { "Krasicki", "Słowacki" }));
                //allQuestions.Add(new Question("Z którego roku pochodzi dzwon w wieży ratusza?", "/Assets/Photos/ratusz.JPG", "1368", new[] { "1544", "1829" }));
                //allQuestions.Add(new Question("Ile stopni prowadzi na górę wieży widokowej kościoła garnizonowego?", "/Assets/Photos/wieza.JPG", "302", new[] { "278", "294" }));
                //allQuestions.Add(new Question("Od rocznicy jakiego wydarzenia bierze nazwę Hala Stulecia?", "/Assets/Photos/hala_stulecia.JPG", "Bitwy pod Lipskiem", new[] { "Wjazdu Napoleona", "Otwarcia ZOO" }));
                allQuestions.Add(new Question("What is it?", "/Assets/Photos/panorama_roclawicka.JPG", "Rocławice Panorama", new[] { "The City Museum", "Sports Hall" }));
                allQuestions.Add(new Question("What is in this building?", "/Assets/Photos/urzad_woj.JPG", "Voivodeship Office", new[] { "Parliment", "Office of Wroclaw City" }));
                allQuestions.Add(new Question("Whose is this monument?", "/Assets/Photos/jan.jpg", "John XXIII", new[] { "John  Paul II", "Pius XII" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/poczta_polska.JPG", "192", new[] { "124", "133" }));

                //Culture
                allQuestions.Add(new Question("Ile kinowych multiplexów jest we Wrocławiu?", "/Assets/Photos/Wroclaw.png", "5", new[] { "10", "2" }));
                allQuestions.Add(new Question("Jak nazywa się wrocławski teatr muzyczny?", "/Assets/Photos/capitol.jpg", "Capitol", new[] { "Broadway", "Grease" }));
                allQuestions.Add(new Question("Przy jakiej ulicy mieści się Kino Nowe Horyzonty?", "/Assets/Photos/Wroclaw.png", "Kazimierza Wielkiego", new[] { "Józefa Poniatowskiego", "Legnicka" }));
                allQuestions.Add(new Question("Jaki utwór gramy podczas bicia rekordu Guinessa na rynku?", "/Assets/Photos/Wroclaw.png", "Hey Joe", new[] { "Bold as Love", "Fire" }));
                allQuestions.Add(new Question("Który z festiwali odbywa się we Wrocławiu?", "/Assets/Photos/Wroclaw.png", "Jazz nad Odrą", new[] { "Live Festival", "Jazz Jamboree" }));
                allQuestions.Add(new Question("Co ginie z fontanny na placu Uniwersyteckim?", "/Assets/Photos/szermierz.JPG", "Szpada", new[] { "Dysk", "Pióro" }));
                allQuestions.Add(new Question("W którym roku Wrocław będzie Europejską Stolicą Kultury?", "/Assets/Photos/stolica_kult.jpg", "2016", new[] { "2019", "2017" }));
                allQuestions.Add(new Question("Co to za budynek?", "/Assets/Photos/opera.JPG", "Opera", new[] { "Teatr", "Kino" }));
                allQuestions.Add(new Question("W którym miesiącu zacznie się festiwal Szanty we Wrocławiu?", "/Assets/Photos/szanty.JPG", "Luty", new[] { "Marzec", "Lipiec" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/PocztaPolska.JPG", "?", new[] { "124", "133" }));

                //Sport
                allQuestions.Add(new Question("Ile jest miejsc na stadionie miejskim?", "/Assets/Photos/Wroclaw.png", "ok. 45 tys.", new[] { "ok. 22 tys.", "ok. 38 tys." }));
                allQuestions.Add(new Question("Ile Mistrzostw Polski ma na koncie WKS (koszykówka)?", "/Assets/Photos/Wroclaw.png", "17", new[] { "11", "15" }));
                allQuestions.Add(new Question("W którym roku WKS (piłka nożna) zdobył Mistrzostwo Polski?", "/Assets/Photos/Wroclaw.png", "2012", new[] { "2011", "2009" }));
                allQuestions.Add(new Question("Ile Wicemistrzostw Polski ma na koncie WKS (piłka nożna)?", "/Assets/Photos/Wroclaw.png", "3", new[] { "9", "2" }));
                allQuestions.Add(new Question("Który z bramkarzy WKS zazwyczaj występuje w pierwszej jedynastce?", "/Assets/Photos/Wroclaw.png", "M. Kelemen", new[] { "W. Pawłowski", "J. Wrąbel" }));
                allQuestions.Add(new Question("Od którego sezonu Impel Wrocław (siatkówka) gra w ekstralidze?", "/Assets/Photos/Wroclaw.JPG", "2012/2013", new[] { "2011/2012", "2009/2010" }));
                //allQuestions.Add(new Question("W którym roku Wrocław będzie Europejską Stolicą Kultury?", "/Assets/Photos/stolica_kult.jpg", "2016", new[] { "2019", "2017" }));
                //allQuestions.Add(new Question("Co to za budynek?", "/Assets/Photos/opera.JPG", "Opera", new[] { "Teatr", "Kino" }));
                //allQuestions.Add(new Question("W którym miesiącu zacznie się festiwal Szanty we Wrocławiu?", "/Assets/Photos/PocztaPolska.JPG", "Lutym", new[] { "Marcu", "Lipcu" }));
                //allQuestions.Add(new Question("?", "/Assets/Photos/PocztaPolska.JPG", "?", new[] { "124", "133" }));
            }


            ALL_QUESTION_COUNT = allQuestions.Count();



            StartGame();

        }


        private void StartGame()
        {
            goodAnswer = 0;
            currentQuestionCount = 0;
            currentQuestion = null;
            playQuestions = TakeQuestions(new List<Question>(allQuestions));

            NextQuestion();
        }

        private List<Question> TakeQuestions(List<Question> aList)
        {
            Random r = new Random();
            List<Question> list = new List<Question>();
            int indexRandom = 0;

            for (int i = 0; i < PLAY_QUESTIONS_COUNT; i++)
            {
                indexRandom = r.Next(0,aList.Count);
                list.Add(aList[indexRandom]);
                aList.RemoveAt(indexRandom);
            }

            return list;
        }

        private async void CheckAnswer(string answer)
        {
            if (answer.Equals(currentQuestion.trueAnswer))
            {
                goodAnswer++;
                answerPopupStack.Background = new SolidColorBrush(Colors.Green);
                answerPopupText.Text = "Dobrze";
                answerPopupText.FontSize = 100;
            }
            else
            {
                answerPopupStack.Background = new SolidColorBrush(Colors.Red);
                answerPopupText.Text = "Źle";
                answerPopupText.FontSize = 200;
            }
            answerPopup.IsOpen = true;
            await Task.Delay(500);
            answerPopup.IsOpen = false;

            NextQuestion();
        }


        private void NextQuestion()
        {
            if (currentQuestionCount < PLAY_QUESTIONS_COUNT)
            {
                currentQuestion = playQuestions[currentQuestionCount];

                questionText.Text = currentQuestion.questionText;

                questionImage.Source = new BitmapImage(new Uri(currentQuestion.imagePath,UriKind.Relative));

                List<string> answers = new List<string>(3);
                answers = Shuffle<string>(new List<string> { currentQuestion.trueAnswer, currentQuestion.falseAnswer[0], currentQuestion.falseAnswer[1] });
                answerFirst.Content =  answers[0];
                answerSecond.Content = answers[1];
                answerThird.Content = answers[2];
                
                currentQuestionCount++;

                countText.Text = currentQuestionCount + "/" + PLAY_QUESTIONS_COUNT;
            }
            else
            {
                NavigationService.Navigate(new Uri("/ResultsPage.xaml?result=" + goodAnswer + "/" + PLAY_QUESTIONS_COUNT + "&percent=" + (float)goodAnswer/(float)PLAY_QUESTIONS_COUNT*100, UriKind.Relative));            
            }
        }

        private List<T> Shuffle<T>(List<T> inputList)
        {
            List<T> randomList = new List<T>(inputList.Count);

            Random random = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = random.Next(0, inputList.Count);
                randomList.Add(inputList[randomIndex]);
                inputList.RemoveAt(randomIndex);
            }

            return randomList;
        }


        public class Question
        {
            public string trueAnswer {get;set;}
            public string[] falseAnswer {get;set;}
            public string questionText {get;set;}
            public string imagePath {get;set;}

            public Question(string qT, string iP, string tA, string[] fA)
            {
                questionText = qT;
                imagePath = iP;
                trueAnswer = tA;
                falseAnswer = fA;
            }

            public Question(Question quest)
            {
                questionText = quest.questionText;
                imagePath = quest.imagePath;
                trueAnswer = quest.trueAnswer;
                falseAnswer = quest.falseAnswer;
            }


        }

        private void answerFirst_Click(object sender, RoutedEventArgs e)
        {
            CheckAnswer((sender as Button).Content as string);
        }

        private void answerSecond_Click(object sender, RoutedEventArgs e)
        {
            CheckAnswer((sender as Button).Content as string);
        }

        private void answerThird_Click(object sender, RoutedEventArgs e)
        {
            CheckAnswer((sender as Button).Content as string);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Back && App.isNewGame)
            {
                App.isNewGame = false;
                StartGame();
            }
        }



    }
}