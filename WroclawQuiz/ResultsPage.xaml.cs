﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using WroclawQuiz.Resources;


namespace WroclawQuiz
{
    
    

    public partial class ResultsPage : PhoneApplicationPage
    {
        string BAD = AppResources.Bad;
        string GOOD = AppResources.Good;
        string BETTER = AppResources.Better;
        string PERFECT = AppResources.Perfect;
        string percent = "";
        
        public ResultsPage()
        {
            InitializeComponent();
            ResultsComment();
        }


        private void ResultsComment()
        {
            
           

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string result = "";
            
            if(NavigationContext.QueryString.TryGetValue("result",out result))
                resultsText.Text = result;
            if (NavigationContext.QueryString.TryGetValue("percent", out percent))
                resultsText.Text += " " + percent + "%";

            int procent = int.Parse(percent);
            if(procent <= 40)
                commentText.Text = BAD;
            else
                if(procent <= 75)
              
                    commentText.Text = GOOD;
                else
                    if(procent <= 99)
                        commentText.Text = BETTER;
                    else
                        if(procent <= 100)
                            commentText.Text = PERFECT;
                 
            



        }



        private void PlayAgainButton_Click(object sender, RoutedEventArgs e)
        {
            App.isNewGame = true;
            NavigationService.GoBack();
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            e.Cancel = true;
            NavigationService.RemoveBackEntry();
            NavigationService.GoBack();
            
        }

    }
}